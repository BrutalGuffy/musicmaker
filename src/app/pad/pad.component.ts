import {Component, Input, OnInit} from '@angular/core';
import {loadAudioBuffer} from 'audiobuffer-loader';

@Component({
  selector: 'app-pad',
  templateUrl: './pad.component.html',
  styleUrls: ['./pad.component.css']
})
export class PadComponent implements OnInit {
  @Input() pad: any;
  @Input() keyCode: any;

  constructor() {

  }

  ngOnInit() {
    const keyCode = this.pad.key;
    const note = this.pad.note;
    const velocity = this.pad.velocity;
    const link = this.pad.link;
    console.log('im in pad!', this.pad.link);
    const sample = new Tone.Player({
      url : link,
      'autostart' : false,
    }).toMaster();

    addEventListener('keydown', event => {
      if (keyCode === event.keyCode && !event.repeat) {
        sample.start();
        // note.triggerAttack(velocity);
        this.pad.isActive = true;
      }
    });

    addEventListener('keyup', eventDown => {
      if (keyCode === eventDown.keyCode) {
        sample.stop();
        // note.triggerRelease(velocity);
        this.pad.isActive = false;
      }
    });
  }

}

// const ctx = new AudioContext();
// const sound = ctx.createBuffer(2, 10000, 44100);
// const sample = new Tone.Sampler({
//   'A4' : 'audio/Pad1.wav',
// }, function() {
//   sample.triggerAttack('A4');
// }).toMaster();


