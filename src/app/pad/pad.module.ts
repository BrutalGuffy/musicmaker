import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PadComponent } from './pad.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [PadComponent],
  declarations: [PadComponent]
})
export class PadModule { }
