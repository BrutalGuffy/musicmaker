import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoopComponent } from './loop.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [LoopComponent],
  declarations: [LoopComponent]
})
export class LoopModule { }
