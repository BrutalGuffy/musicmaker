import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-loop',
  templateUrl: './loop.component.html',
  styleUrls: ['./loop.component.css']
})
export class LoopComponent implements OnInit {
  @Output() note = new EventEmitter<any>();
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      velocity: ['A4', ],
      duration: '4n',
    });

    this.form.valueChanges
      .subscribe((value) => {
      });
  }

  onSubmit() {
    event.preventDefault();
    const note = new Tone.PolySynth().toMaster();
    this.note.emit({note: note, velocity: this.form.value.velocity, duration: this.form.value.duration});
  }

}
