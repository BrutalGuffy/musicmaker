import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SequenceComponent } from './sequence.component';
import {LoopModule} from '../loop/loop.module';
import { StepComponent } from './step/step.component';
import {StepModule} from './step/step.module';

@NgModule({
  imports: [
    CommonModule,
    LoopModule,
    StepModule,
  ],
  exports: [SequenceComponent, StepComponent],
  declarations: [SequenceComponent, StepComponent]
})
export class SequenceModule { }
