import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sequence',
  templateUrl: './sequence.component.html',
  styleUrls: ['./sequence.component.css']
})
export class SequenceComponent implements OnInit {
  @Input() note: any;
  @Output() sequence = new EventEmitter<any>();
  steps = [0, 1, 2, 3];
  test = [false, false, false, false];

  constructor() { }

  ngOnInit() {
    // const seq = new Tone.Sequence((time, col) => {
    //   if (this.test[col]) {
    //     this.note.note.triggerAttackRelease(this.note.velocity, '8n');
    //   }
    // }, this.steps, '8n');
    // this.sequence.emit(seq);

    const duration = this.note.duration;
    const sample = new Tone.Player({
      url : 'https://firebasestorage.googleapis.com/v0/b/musicmaker-809ce.appspot.com/o/Pad1.wav?alt=media&token=c3804ae2-585e-472d-b896-eed80df7e4db',
      'autostart' : false,
    }).toMaster();
    const seq = new Tone.Sequence((time, col) => {
      if (this.test[col]) {
        sample.start(time, 0, duration);
      }
    }, this.steps);
    this.sequence.emit(seq);
  }

  onChangeState(event) {
    this.test[event.index] = !this.test[event.index];
    console.log(this.test[event.index]);
  }

}
