import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css']
})
export class StepComponent implements OnInit {
  @Input() step: any;
  @Input() i: number;
  @Output() state = new EventEmitter<any>();
  isActive = false;

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.isActive = !this.isActive;
    console.log(this.isActive);
    this.state.emit({index: this.i, isActive: this.isActive});
  }

}

