import {EventEmitter, Injectable, Output} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {FirebaseListObservable} from '@angular/fire/database-deprecated';
import {Upload} from './upload';
import * as firebase from 'firebase';
import {from, Observable, Subject} from 'rxjs';

@Injectable()
export class UploadService {
  link = '';

  private answer$ = new Subject();

  constructor(private db: AngularFireDatabase) { }

  private basePath: string = '/uploads';
  uploads: FirebaseListObservable<Upload[]>;

  pushUpload(upload: Upload): Observable<any> {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) =>  {
        upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;
      },
      (error) => {
        console.log(error);
      },
      () => {
        upload.name = upload.file.name;
        const event$ = from(uploadTask.snapshot.ref.getDownloadURL());

        event$.subscribe(this.answer$);

        uploadTask.snapshot.ref.getDownloadURL().then(downloadUrl => {
          upload.url = downloadUrl;
          this.link = downloadUrl;
          // this.saveFileData(upload);
        });
      }
    );

    return this.answer$;
  }

  // private saveFileData(upload: Upload) {
  //   this.db.list(`${this.basePath}/`).push(upload);
  // }
}
