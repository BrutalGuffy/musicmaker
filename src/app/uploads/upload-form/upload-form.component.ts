import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import { UploadService } from '../shared/upload.service';
import { Upload } from '../shared/upload';
import * as _ from 'lodash';
import {Subscription} from 'rxjs';

@Component({
  selector: 'upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.css']
})
export class UploadFormComponent implements OnInit {
  @Output() gotLink = new EventEmitter<any>();
  selectedFiles: FileList;
  currentUpload: Upload;

  constructor(private upSvc: UploadService) { }

  ngOnInit() {
  }

  detectFiles(event) {
    this.selectedFiles = event.target.files;
  }

  uploadSingle() {
    event.preventDefault();
    const file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);
    const answer$ = this.upSvc.pushUpload(this.currentUpload);
    answer$.subscribe(data => {
      this.gotLink.emit(data);
    });
  }

}

