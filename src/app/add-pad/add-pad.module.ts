import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPadComponent } from './add-pad.component';
import { ReactiveFormsModule } from '@angular/forms';
import {UploadFormComponent} from '../uploads/upload-form/upload-form.component';
import {UploadService} from '../uploads/shared/upload.service';
import { MatSelectModule } from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule
  ],
  exports: [AddPadComponent, UploadFormComponent],
  declarations: [AddPadComponent, UploadFormComponent],
  providers: [UploadService],
})
export class AddPadModule { }
