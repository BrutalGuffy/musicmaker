import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-add-pad',
  templateUrl: './add-pad.component.html',
  styleUrls: ['./add-pad.component.css']
})
export class AddPadComponent implements OnInit {
  @Output() addPad = new EventEmitter<any>();
  soundSource = [
    {value: 'custom', viewValue: 'Add custom sample'},
    {value: 'polySynth', viewValue: 'Poly Synth'}
  ];

  form: FormGroup;
  isLinkReady: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    }

  ngOnInit() {
    this.form = this.formBuilder.group({
      soundSource: null,
      key: null,
      velocity: ['A4', ],
      link: '',
    });

    this.form.valueChanges
      .subscribe((value) => {
        console.log(value);
      });
  }

  onLink(event) {
    this.form.controls.link.setValue(event);
    this.isLinkReady = true;
  }

  onChange(event) {
    this.form.controls.key.setValue(event.keyCode);
  }

  onGo() {
    event.preventDefault();
    console.log(this.form.value.file);
    event.preventDefault();
    const note = new Tone.PolySynth().toMaster();
    console.log(this.form.value.link);
    const pad = {note: note, key: this.form.value.key, isActive: false, velocity: this.form.value.velocity, link: this.form.value.link};
    this.isLinkReady = false;
    this.addPad.emit(pad);
    this.form.reset();
  }

}
