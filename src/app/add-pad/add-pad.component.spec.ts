import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPadComponent } from './add-pad.component';

describe('AddPadComponent', () => {
  let component: AddPadComponent;
  let fixture: ComponentFixture<AddPadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
