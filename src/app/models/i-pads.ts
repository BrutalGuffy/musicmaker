export interface IPads {
  sound: any;
  keyCode: number;
  isActive: boolean;
  velocity: string;
}
 export interface INote {
  keyCode?: number;
  isActive?: number;
  note?: string;
  soundSource: string;
  link?: string;
 }
