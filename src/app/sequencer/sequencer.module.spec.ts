import { SequencerModule } from './sequencer.module';

describe('SequencerModule', () => {
  let sequencerModule: SequencerModule;

  beforeEach(() => {
    sequencerModule = new SequencerModule();
  });

  it('should create an instance', () => {
    expect(sequencerModule).toBeTruthy();
  });
});
