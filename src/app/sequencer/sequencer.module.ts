import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SequencerComponent } from './sequencer.component';
import {SequenceModule} from '../sequence/sequence.module';
import {LoopModule} from '../loop/loop.module';

@NgModule({
  imports: [
    CommonModule,
    SequenceModule,
    LoopModule,
  ],
  exports: [SequencerComponent],
  declarations: [SequencerComponent]
})
export class SequencerModule { }
