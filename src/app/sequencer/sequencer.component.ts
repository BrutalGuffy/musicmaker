import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-sequencer',
  templateUrl: './sequencer.component.html',
  styleUrls: ['./sequencer.component.css']
})
export class SequencerComponent implements OnInit {
  sequences = [];
  notes = [];
  isActive = false;

  constructor() { }

  ngOnInit() {
  }

  onAddNote(note) {
    this.notes.push(note);
    console.log(this.notes);
  }

  onSubmit() {
    event.preventDefault();
    this.isActive = !this.isActive;
    Tone.Transport.start();
    if (this.isActive) {
      this.sequences.forEach(sequence => {
        sequence.start();
      });
    } else {
      this.sequences.forEach(sequence => {
        sequence.stop();
      });
    }
  }

  onAddSequence(seq) {
    this.sequences.push(seq);
  }

}
