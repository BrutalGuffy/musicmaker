import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PadModule } from './pad/pad.module';
import { AddPadModule } from './add-pad/add-pad.module';
import { PadListModule } from './pad-list/pad-list.module';
import { SequenceModule } from './sequence/sequence.module';
import {SequencerModule} from './sequencer/sequencer.module';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireModule} from '@angular/fire';
import {firebaseConfig} from './environments/firebase.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PadModule,
    AddPadModule,
    PadListModule,
    SequenceModule,
    SequencerModule,
    AngularFireModule.initializeApp(firebaseConfig),
    BrowserAnimationsModule
  ],
  providers: [
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
