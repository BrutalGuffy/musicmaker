import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PadListComponent } from './pad-list.component';
import { PadModule } from '../pad/pad.module';
import { AddPadModule } from '../add-pad/add-pad.module';

@NgModule({
  imports: [
    CommonModule,
    PadModule,
    AddPadModule,
  ],
  exports: [PadListComponent],
  declarations: [PadListComponent]
})
export class PadListModule { }
