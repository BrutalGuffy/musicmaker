import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IPads} from '../models/i-pads';

@Component({
  selector: 'app-pad-list',
  templateUrl: './pad-list.component.html',
  styleUrls: ['./pad-list.component.css']
})
export class PadListComponent implements OnInit {
  @Output() keyCode = new EventEmitter<number>();
  pads: IPads[] = [];

  constructor() { }

  ngOnInit() {

  }

  onAddPad(pad: IPads) {
    this.pads.push(pad);
    console.log(this.pads);
  }

}
